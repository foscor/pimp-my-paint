/*
** my.h for mytmp in /home/tahar_w/my_paint
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Thu Jun 19 12:59:40 2014 tahar_w
** Last update Thu Jun 19 13:01:59 2014 tahar_w
*/

#ifndef MY_H_
# define MY_H_

/*
** BASIC FUNCTS
*/

void	my_putchar(char );
void	my_putstr(char *);
int	my_strlen(char *);

#endif /* !MY_H_ */
