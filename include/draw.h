/*
** draw.h for fdf in /home/tahar_w/rendu/MUL_2013_fdf
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Sun Dec  8 21:28:58 2013 tahar_w
** Last update Mon Jun 23 21:32:44 2014 tahar_w
*/

#ifndef DRAW_H_
# define DRAW_H_

# define LINE		0
# define CARRE		1
# define TRIANGLE	2
# define CIRCLE		3
# define NOIR		0x000000
# define BLANC		0xFFFFFF
# define ROUGE		0xFF0000
# define BLEU		0x0000FF
# define VERT		0x00FF00
# define JAUNE		0xFFFF00
# define SMAL		4
# define NARM		5
# define BIG		6

/*
** STRUCT FUNCTS DRAW LINE
*/

struct		s_draw
{
  int		x;
  int		y;
  int		swap;
  void		*window;
  void		*mlx_ptr;
};

/*
** STRUCT PARAM MLX
*/

struct		s_param
{
  void		*window;
  void		*mlx_ptr;
  void		*win_color;
  void		*img_ptr;
  void		*img_color;
  void		*img_select;
  char		*data;
  int		win_x;
  int		win_y;
  int		bpp;
  int		sizeline;
  int		endian;
  int		k_code;
  int		color;
  int		fill;
  int		thick;
  int		size;
  struct	s_draw pt1;
  struct	s_draw pt2;
};

/*
** STRUCT SAVE for POS ONE of X, Y
*/

struct		s_save
{
  int		x;
  int		y;
  int		count;
};

typedef struct	s_draw t_draw;
typedef struct	s_param t_param;
typedef struct	s_save t_save;

/*
** DRAW LINE FUNCTS
*/

int	draw_line(t_draw pt1, t_draw pt2, t_param *param);
int	draw_line1(t_draw pt1, t_draw pt2, t_param *param);
int	draw_line2(t_draw pt1, t_draw pt2, t_param *param);


/*
** GEO FUNCTS
*/

void	my_triangle(t_param *param, t_draw pt1, t_draw pt2, int size);
void	my_circle(t_param *param, t_draw pt1, int rayon, int color);
void	my_carre(t_param *param, t_draw pt1, t_draw pt2, int size);
void	m_draw_line(t_param *param, t_draw pt1, t_draw pt2);

/*
** MLX FUNCTS
*/

int	start_key(int keycode);
int	start_expose2(t_param *param);

/*
** EXPOSE FUNCTS
*/

void	expose_circle(t_param *param, t_draw pt1);
void	expose_carre(t_param *param, t_draw pt1, t_draw pt2);
void	expose_triangle(t_param *param, t_draw pt1, t_draw pt2);
void	expose_draw_line(t_param *param, t_draw pt1, t_draw pt2);
void	expose_fill(t_param *param, t_draw pt1);
void	expose_thick(t_param *param, t_draw pt1, t_draw pt2);
void	tools_expose(t_param *param, t_draw pt1, t_draw pt2);
int	start_expose(t_param *param, t_draw pt1, t_draw pt2);

/*
** CHOSE GEO FUNCTS
*/

void	chose_geo_fill(t_param *param, t_draw pt1, t_draw pt2);
void	chose_geo_carre(t_param *param, t_draw pt1, t_draw pt2);
void	chose_geo_circle(t_param *param, t_draw pt1);
void	chose_geo_triangle(t_param *param, t_draw pt1, t_draw pt2);
int	chose_geo(t_param *param, t_draw pt1, t_draw pt2, int count);

/*
** GEST MENU FUNCTS
*/

int	gest_fill(t_param *param);
int	gest_thick(t_param *param);
int	click_menu(t_param *param, int x, int y, int bouton);
int	gest_select_geo(t_param *param, int x, int y);
int	write_pixel(int bouton, int x, int y, t_param *param);
void	save_cor_line(int *save_x, int *save_y, int x, int y);

/*
** MY_TOOLS FUNTS
*/

int	start_expose2(t_param *param);
int	start_key2(int keycode);
int	chose_color(int bouton, int x, int y, t_param *param);
int	select_color(t_param *param);

/*
** MY_WALL FUNCTS
*/

void	wall_init(t_param *param);
void	color_tools(t_param *param);

/*
** CREAT_MENU FUNCTS
*/

void	between_tools(t_param *param, int *i, int x, int y);
void	creat_menu3(t_param *param, int *i);
void	creat_menu2(t_param *param , int *i);
void	creat_menu1(t_param *param, int *i);
void	creat_menu(t_param *param, int *i);

/*
** COLOR_MENU FUNCTS
*/

void	chose_color2(t_param *param, int *i);
void	chose_color1(t_param *param, int *i);
void	color_menu(t_param *param);

/*
** CHOSE GEO FUNCTS
*/

void	chose_geo_fill(t_param *param, t_draw pt1, t_draw pt2);
void	chose_geo_carre(t_param *param, t_draw pt1, t_draw pt2);
void	chose_geo_circle(t_param *param, t_draw pt1);
void	chose_geo_triangle(t_param *param, t_draw pt1, t_draw pt2);
int	chose_geo(t_param *param, t_draw pt1, t_draw pt2, int count);

/*
** MY_THICK FUNCTS
*/

void	carre_thick(t_param *param, t_draw pt1, t_draw pt2, int thick);
void	triangle_thick(t_param *param, t_draw pt1, t_draw pt2, int thick);
void	circle_thick(t_param *param, t_draw pt1, int thick);

/*
** MY_FILL FUNCTS
*/

void	fill_circle(t_param *param, t_draw pt1, int size);
void	fill_tri(t_param *param, t_draw pt1, t_draw pt2 ,int size);
void	fill_carre(t_param *param, t_draw pt1, t_draw pt2, int size);

/*
** DYNA_COLOR FUNCTS
*/

void	dyna_menu(t_param *param, t_draw pt1, t_draw pt2);
void	dyna_fill(t_param *param, t_draw pt1, int color);
void	dyna_line(t_param *param, t_draw pt1, t_draw pt2, int color);
void	dyna_circle(t_param *param, t_draw pt1, int color);
void	dyna_triangle(t_param *param, t_draw pt1, t_draw pt2, int color);
void	dyna_carre(t_param *param, t_draw pt1, t_draw pt2, int color);
void	dyna_thick(t_param *param, t_draw pt1, t_draw pt2, int color);


#define ABS(x,y) ((x > y) ? (x - y) : (y - x))

#endif /* !DRAW_H_ */
