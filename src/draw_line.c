/*
** draw_line.c for pmp in /home/tahar_w/my_paint
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Thu Jun 19 16:12:06 2014 tahar_w
** Last update Sun Jun 22 01:39:34 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

int     draw_line(t_draw pt1, t_draw pt2, t_param *param)
{
  t_draw        swap;

  if (ABS(pt1.x, pt2.x) >= ABS(pt1.y, pt2.y))
    {
      if (pt1.x >= pt2.x)
        {
          swap = pt1;
          pt1 = pt2;
          pt2 = swap;
          draw_line1(pt1, pt2, param);
        }
      draw_line1(pt1, pt2, param);
    }
  else
    {
      if (pt1.y >= pt2.y)
        {
          swap = pt1;
          pt1 = pt2;
          pt2 = swap;
          draw_line2(pt1, pt2, param);
        }
      draw_line2(pt1, pt2, param);
    }
  return (0);
}

int     draw_line1(t_draw pt1, t_draw pt2, t_param *param)
{
  int   x;

  x = pt1.x;
  while (x <= pt2.x)
    {
      mlx_pixel_put(param->mlx_ptr, param->window,x,pt1.y
		    +((pt2.y-pt1.y)*(x-pt1.x))/(pt2.x-pt1.x), param->color);
      x++;
    }
  return (0);
}

int     draw_line2(t_draw pt1, t_draw pt2, t_param *param)
{
  int   y;

  y = pt1.y;
  while (y <= pt2.y)
    {
      mlx_pixel_put(param->mlx_ptr, param->window,((pt2.x-pt1.x)*(y-pt1.y))
		    /(pt2.y-pt1.y)+pt1.x, y, param->color);
      y++;
    }
  return (0);
}
