/*
** my_thick.c for pimp my paint in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Sun Jun 22 21:18:06 2014 tahar_w
** Last update Mon Jun 23 16:54:39 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	carre_thick(t_param *param, t_draw pt1, t_draw pt2, int thick)
{
  int	i;
  int	size;

  i = 0;
  size = param->size;
  while (i <= thick)
    {
      my_carre(param, pt1, pt2, (size + i));
      pt1.x--;
      pt1.y--;
      i = i + 2;
    }
}

void	triangle_thick(t_param *param, t_draw pt1, t_draw pt2, int thick)
{
  int	i;

  i = 0;
  while (i <= thick)
    {
      my_triangle(param, pt1, pt2, 50 + i);
      pt1.x--;
      pt1.y--;
      i = i + 2;
      pt1.x++;
    }
}

void	circle_thick(t_param *param, t_draw pt1, int thick)
{
  int	i;

  i = 0;
  while (i <= thick)
    {
      my_circle(param, pt1, 50 + i, param->color);
      i++;
    }
  i = 0;
  pt1.x++;
  while (i <= thick)
    {
      my_circle(param, pt1, 50 + i, param->color);
      i++;
    }
  pt1.y++;
  i = 0;
  while (i <= thick)
    {
      my_circle(param, pt1, 50 + i, param->color);
      i++;
    }
}
