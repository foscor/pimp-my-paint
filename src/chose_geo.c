/*
** chose_geo.c for pmp in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Mon Jun 23 16:15:17 2014 tahar_w
** Last update Mon Jun 23 21:31:15 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	chose_geo_fill(t_param *param, t_draw pt1, t_draw pt2)
{
  if ((param->k_code == CARRE) && (param->fill == 1))
    fill_carre(param, pt1, pt2, 100);
  else if ((param->k_code == CIRCLE) && (param->fill == 1))
    fill_circle(param, pt1, 50);
  else if ((param->k_code == TRIANGLE) && (param->fill == 1))
    fill_tri(param, pt1, pt2, 100);
}

void	chose_geo_carre(t_param *param, t_draw pt1, t_draw pt2)
{
  if (param->k_code == CARRE)
    {
      if (param->thick == SMAL)
        my_carre(param, pt1, pt2, 100);
      else if (param->thick == NARM)
        carre_thick(param, pt1, pt2, 15);
      else if (param->thick == BIG)
        carre_thick(param, pt1, pt2, 30);
    }
}

void	chose_geo_circle(t_param *param, t_draw pt1)
{
  if (param->k_code == CIRCLE)
    {
      if (param->thick == SMAL)
        my_circle(param, pt1, 50, param->color);
      else if (param->thick == NARM)
        circle_thick(param, pt1, 10);
      else if (param->thick == BIG)
        circle_thick(param, pt1, 20);

    }
}

void	chose_geo_triangle(t_param *param, t_draw pt1, t_draw pt2)
{
  if (param->k_code == TRIANGLE)
    {
      if (param->thick == SMAL)
        my_triangle(param, pt1, pt2, 100);
      else if (param->thick == NARM)
        triangle_thick(param, pt1, pt2, 20);
      else if (param->thick == BIG)
        triangle_thick(param, pt1, pt2, 50);
    }
}

int	chose_geo(t_param *param, t_draw pt1, t_draw pt2, int count)
{
  param->size = 50;
  if (param->k_code == LINE)
    {

      if ((count % 2) == 0)
        {
          if (pt1.x != pt2.x && pt1.y != pt2.y)
            draw_line(pt1, pt2, param);
        }
      count++;
    }
  chose_geo_carre(param, pt1, pt2);
  chose_geo_circle(param, pt1);
  chose_geo_triangle(param, pt1, pt2);
  chose_geo_fill(param, pt1, pt2);
  dyna_menu(param, pt1, pt2);
  return (count);
}
