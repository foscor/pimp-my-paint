/*
** dyna_color.c for pmp in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Mon Jun 23 21:29:03 2014 tahar_w
** Last update Mon Jun 23 21:31:47 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	dyna_carre(t_param *param, t_draw pt1, t_draw pt2, int color)
{
  if (param->k_code == CARRE)
    param->color = ROUGE;
  else
    param->color = BLANC;
  pt1.x = 715;
  pt1.y = 125;
  my_carre(param, pt1, pt2, 70);
  param->color = BLANC;
  mlx_expose_hook(param->window, start_expose, param);
  param->color = color;
}

void	dyna_thick(t_param *param, t_draw pt1, t_draw pt2, int color)
{
  if (param->thick == SMAL)
    param->color = ROUGE;
  else
    param->color = BLANC;
  pt1.x = 715;
  pt1.y = 650;
  my_carre(param, pt1, pt2, 40);
  pt1.x = 730;
  pt1.y = 670;
  if (param->thick == NARM)
    param->color = ROUGE;
  else
    param->color = BLANC;
  param->size = 30;
  carre_thick(param, pt1, pt2, 10);
  pt1.x = 750;
  pt1.y = 690;
  if (param->thick == BIG)
    param->color = ROUGE;
  else
    param->color = BLANC;
  carre_thick(param, pt1, pt2, 20);
  param->color = BLANC;
  mlx_expose_hook(param->window, start_expose, param);
  param->color = color;
}
