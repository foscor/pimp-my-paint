/*
** dyna_color_main.c for pmp in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Mon Jun 23 21:23:18 2014 tahar_w
** Last update Mon Jun 23 21:32:30 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	dyna_triangle(t_param *param, t_draw pt1, t_draw pt2, int color)
{
  if (param->k_code == TRIANGLE)
    param->color = ROUGE;
  else
    param->color = BLANC;
  pt1.x = 750;
  pt1.y = 225;
  my_triangle(param, pt1, pt2, 70);
  param->color = BLANC;
  mlx_expose_hook(param->window, start_expose, param);
  param->color = color;
}

void	dyna_circle(t_param *param, t_draw pt1, int color)
{
  pt1.x = 750;
  pt1.y = 370;
  if (param->k_code == CIRCLE)
    my_circle(param, pt1, 40, ROUGE);
  else
    my_circle(param, pt1, 40, BLANC);
  mlx_expose_hook(param->window, start_expose, param);
  param->color = color;
}

void	dyna_line(t_param *param, t_draw pt1, t_draw pt2, int color)
{
  if (param->k_code == LINE)
    param->color = ROUGE;
  else
    param->color = BLANC;
  pt1.x = 715;
  pt1.y = 480;
  pt2.x = 785;
  pt2.y = 480;
  draw_line(pt1, pt2, param);
  param->color = BLANC;
  mlx_expose_hook(param->window, start_expose, param);
  param->color = color;
}

void	dyna_fill(t_param *param, t_draw pt1, int color)
{
  if (param->fill == 1)
    param->color = ROUGE;
  else
    param->color = BLANC;
  pt1.x = 740;
  pt1.y = 570;
  fill_circle(param, pt1, 28);
  pt1.x = 765;
  pt1.y = 585;
  my_circle(param, pt1, 28, BLANC);
  param->color = BLANC;
  mlx_expose_hook(param->window, start_expose, param);
  param->color = color;
}

void	dyna_menu(t_param *param, t_draw pt1, t_draw pt2)
{
  int	color_init;

  color_init = param->color;
  dyna_circle(param, pt1, color_init);
  dyna_carre(param, pt1, pt2, color_init);
  dyna_triangle(param, pt1, pt2, color_init);
  dyna_line(param, pt1, pt2, color_init);
  dyna_fill(param, pt1, color_init);
  dyna_thick(param, pt1, pt2, color_init);
}
