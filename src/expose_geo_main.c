/*
** expose_geo_main.c for pmp in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Mon Jun 23 16:03:04 2014 tahar_w
** Last update Mon Jun 23 20:35:31 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	expose_thick(t_param *param, t_draw pt1, t_draw pt2)
{
  pt1.x = 715;
  pt1.y = 650;
  if (param->thick == SMAL)
    param->color = ROUGE;
  else
    param->color = BLANC;
  my_carre(param, pt1, pt2, 40);
  pt1.x = 730;
  pt1.y = 670;
  if (param->thick == NARM)
    param->color = ROUGE;
  else
    param->color = BLANC;
  param->size = 30;
  carre_thick(param, pt1, pt2, 10);
  pt1.x = 750;
  pt1.y = 690;
  if (param->thick == BIG)
    param->color = ROUGE;
  else
    param->color = BLANC;
  carre_thick(param, pt1, pt2, 20);
}

void	tools_expose(t_param *param, t_draw pt1, t_draw pt2)
{
  expose_circle(param, pt1);
  expose_carre(param, pt1, pt2);
  expose_triangle(param, pt1, pt2);
  expose_draw_line(param, pt1, pt2);
  expose_fill(param, pt1);
  expose_thick(param, pt1, pt2);
  param->color = NOIR;
}

int	start_expose(t_param *param, t_draw pt1, t_draw pt2)
{

  mlx_put_image_to_window(param->mlx_ptr, param->window, param->img_ptr,
                          0, 0);

  mlx_put_image_to_window(param->mlx_ptr, param->window, param->img_color,
                          700, 3);
  tools_expose(param, pt1, pt2);
  mlx_string_put(param->mlx_ptr, param->window, 715, 75, BLANC, "MENU COLOR");
  mlx_string_put(param->mlx_ptr, param->window, 715, 20, ROUGE, "MENU TOOLS :");

  return (0);
}
