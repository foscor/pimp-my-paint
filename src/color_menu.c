/*
** color_menu.c for pimp my paint in /home/tahar_w/my_paint/in_progress/src
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Sun Jun 22 11:24:40 2014 tahar_w
** Last update Sun Jun 22 17:31:46 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	chose_color2(t_param *param, int *i)
{
  while (*i != 400 * 100 * 4)
    {
      param->data[*i] = 255;
      param->data[*i + 1] = 0;
      param->data[*i + 2] = 0;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  while (*i != 500 * 100 * 4)
    {
      param->data[*i] = 0;
      param->data[*i + 1] = 255;
      param->data[*i + 2] = 0;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  while (*i != 600 * 100 * 4)
    {
      param->data[*i] = 0;
      param->data[*i + 1] = 255;
      param->data[*i + 2] = 255;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
}

void	chose_color1(t_param *param, int *i)
{
  while (*i != 100 * 100 * 4)
    {
      param->data[*i] = 255;
      param->data[*i + 1] = 255;
      param->data[*i + 2] = 255;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  while (*i != 200 * 100 * 4)
    {
      param->data[*i] = 0;
      param->data[*i + 1] = 0;
      param->data[*i + 2] = 0;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  while (*i != 300 * 100 * 4)
    {
      param->data[*i] = 0;
      param->data[*i + 1] = 0;
      param->data[*i + 2] = 255;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
}

void	color_menu(t_param *param)
{
  int	i;

  i = 0;
  param->img_select = mlx_new_image(param->mlx_ptr, 100, 600);
  param->data = mlx_get_data_addr(param->img_select, &param->bpp
                                  , &param->sizeline, &param->endian);
  chose_color1(param, &i);
  chose_color2(param, &i);
}
