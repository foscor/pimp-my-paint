/*
** creat_geo.c for pimp my paint in /home/tahar_w/my_paint/src
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Fri Jun 20 15:08:22 2014 tahar_w
** Last update Mon Jun 23 16:57:46 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	my_carre(t_param *param, t_draw pt1, t_draw pt2, int size)
{
  int	init_x;

  init_x = pt1.x;
  pt2.x = pt1.x;
  pt2.y = pt1.y + size;
  draw_line(pt1, pt2, param);
  pt2.x = pt1.x + size;
  pt2.y = pt1.y;
  draw_line(pt1, pt2, param);
  pt1.x = pt1.x + size;
  pt2.x = pt1.x;
  pt2.y = pt1.y + size;
  draw_line(pt1, pt2, param);
  pt1.y = pt1.y + size;
  pt1.x = init_x;
  pt2.x = pt1.x + size;
  pt2.y = pt1.y;
  draw_line(pt1, pt2, param);
}

void	my_circle(t_param *param, t_draw pt1, int rayon, int color)
{
  int	d;
  int	y;
  int	x;

  d = 3 - (2 * rayon);
  x = 0;
  y = rayon;
  while (y >= x++)
    {
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x + x, pt1.y + y, color);
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x + y, pt1.y + x, color);
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x - x, pt1.y + y, color);
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x - y, pt1.y + x, color);
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x + x, pt1.y - y, color);
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x + y, pt1.y - x, color);
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x - x, pt1.y - y, color);
      mlx_pixel_put(param->mlx_ptr, param->window, pt1.x - y, pt1.y - x, color);
      if (d < 0)
	d = d + (4 * x) + 6;
      else
	{
	  d = d + 4 * (x - y) + 10;
	  y--;
	}
    }
}

void	my_triangle(t_param *param, t_draw pt1, t_draw pt2, int size)
{
  int	init_x;
  int	init_y;

  init_x = pt1.x;
  init_y = pt1.y;
  pt2.x = pt1.x - (size / 2);
  pt2.y = pt1.y + size;
  draw_line(pt1, pt2, param);
  pt2.x = pt1.x + (size / 2);
  pt2.y = pt1.y + size;
  draw_line(pt1, pt2, param);
  pt1.x = pt1.x - (size / 2);
  pt1.y = pt1.y + size;
  pt2.x = init_x + (size / 2);
  pt2.y = init_y + size;
  draw_line(pt1, pt2, param);
}
