/*
** my_fill.c for pimp my paint in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Sun Jun 22 19:19:02 2014 tahar_w
** Last update Mon Jun 23 17:01:10 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	fill_circle(t_param *param, t_draw pt1, int size)
{
  int	i;

  i = 0;
  while (i <= size)
    {
      my_circle(param, pt1, size - i, param->color);
      i++;
    }
  pt1.x++;
  i = 0;
  while (i <= size)
    {
      my_circle(param, pt1, size - i, param->color);
      i++;
    }
  pt1.y++;
  i = 0;
  while (i <= size)
    {
      my_circle(param, pt1, size - i, param->color);
      i++;
    }
}

void	fill_tri(t_param *param, t_draw pt1, t_draw pt2 ,int size)
{
  int	i;

  i = 4;
  while (i <= size)
    {
      my_triangle(param, pt1, pt2, i);
      i++;
    }
}

void	fill_carre(t_param *param, t_draw pt1, t_draw pt2, int size)
{
  int	i;

  i = 2;
  while (i <= size)
    {
      my_carre(param, pt1, pt2, i);
      i++;
    }
}
