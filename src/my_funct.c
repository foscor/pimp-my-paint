/*
** my_funct.c for my_tmp7 in /home/tahar_w/my_paint
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Thu Jun 19 12:57:49 2014 tahar_w
** Last update Thu Jun 19 13:10:12 2014 tahar_w
*/

#include <unistd.h>
#include "my.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while(str[i])
    i++;
  return (i);
}

void	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}
