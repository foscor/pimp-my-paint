/*
** creat_menu.c for pimp my paint in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Sun Jun 22 01:00:39 2014 tahar_w
** Last update Sun Jun 22 21:10:02 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	between_tools(t_param *param, int *i, int x, int y)
{
  while (*i != x * (y + 5) * 4)
    {
      param->data[*i] = 255;
      param->data[*i + 1] = 255;
      param->data[*i + 2] = 255;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
}

void	creat_menu3(t_param *param, int *i)
{
  while (*i != 100 * 525 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  between_tools(param, i, 100, 525);
  while (*i != 100 * 630 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  between_tools(param, i, 100, 630);
}

void	creat_menu2(t_param *param , int *i)
{
  while (*i != 100 * 315 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  between_tools(param, i, 100, 315);
  while (*i != 100 * 420 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  between_tools(param, i, 100, 420);
}

void	creat_menu1(t_param *param, int *i)
{
  between_tools(param, i, 100, 50);
  while (*i != 100 * 105 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  between_tools(param, i, 100, 105);
  while (*i != 100 * 210 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  between_tools(param, i, 100, 210);
}

void	creat_menu(t_param *param, int *i)
{
  while (*i != 100 * 50 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
  creat_menu1(param, i);
  creat_menu2(param, i);
  creat_menu3(param, i);
  while (*i != 100 * 735 * 4)
    {
      param->data[*i] = 10;
      param->data[*i + 1] = 10;
      param->data[*i + 2] = 10;
      param->data[*i + 3] = 0;
      *i = *i + 4;
    }
}
