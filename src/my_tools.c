
/*
** my_tools.c for pimp my paint in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Sat Jun 21 16:34:40 2014 tahar_w
** Last update Mon Jun 23 18:03:07 2014 tahar_w
*/

#include <mlx.h>
#include <stdio.h>
#include <stdlib.h>
#include "draw.h"

int	start_expose2(t_param *param)
{
  mlx_put_image_to_window(param->mlx_ptr, param->win_color,
                          param->img_select, 0, 0);
  return (0);
}

int	start_key2(int keycode)
{
  if (keycode == 65307)
    exit(EXIT_SUCCESS);
  return (0);
}

int	chose_color(int bouton, int x, int y, t_param *param)
{
  bouton++;
  if ((x >= 0 && x <= 100) && (y >= 0 && y <= 100))
    param->color = BLANC;
  else if ((x >= 0 && x <= 100) && (y >= 100 && y <= 200))
    param->color = NOIR;
  else if ((x >= 0 && x <= 100) && (y >= 200 && y <= 300))
    param->color = ROUGE;
  else if ((x >= 0 && x <= 100) && (y >= 300 && y <= 400))
    param->color = BLEU;
  else if ((x >= 0 && x <= 100) && (y >= 400 && y <= 500))
    param->color = VERT;
  else if ((x >= 0 && x <= 100) && (y >= 500 && y <= 600))
    param->color = JAUNE;
  return (0);
}

int	select_color(t_param *param)
{
  static int	count = 0;

  if (count == 0)
    {
      if ((param->win_color = mlx_new_window
	   (param->mlx_ptr, 100, 600, "Select color")) == NULL)
	exit(EXIT_FAILURE);
      count++;
    }
  color_menu(param);
  mlx_mouse_hook(param->win_color, chose_color, param);
  mlx_key_hook(param->win_color, start_key2, param);
  mlx_expose_hook(param->win_color, start_expose2, param);
  return (1);
}
