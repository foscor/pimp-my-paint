/*
** gest_menu.c for pmp in /home/tahar_w/my_paint/in_progress
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Mon Jun 23 16:36:01 2014 tahar_w
** Last update Mon Jun 23 18:04:14 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

int	gest_fill(t_param *param)
{
  if (param->fill == 0)
    {
      param->fill = 1;
      param->thick = SMAL;
    }
  else
    param->fill = 0;
  return (1);
}

int	gest_thick(t_param *param)
{
  param->fill = 0;
  if (param->thick == SMAL)
    param->thick = NARM;
  else if (param->thick == NARM)
    param->thick = BIG;
  else
    param->thick = SMAL;
  return (1);
}

int	gest_select_geo(t_param *param, int x, int y)
{
  if ((x >= 700 && x <= 800) && (y >= 105 && y <= 210))
    {
      param->k_code = CARRE;
      return (1);
    }
  else if ((x >= 700 && x <= 800) && (y >= 215 && y <= 320))
    {
      param->k_code = TRIANGLE;
      return (1);
    }
  else if ((x >= 700 && x <= 800) && (y >= 325 && y <= 430))
    {
      param->k_code = CIRCLE;
      return (1);
    }
  else if ((x >= 700 && x <= 800) && (y >= 435 && y <= 540))
    {
      param->k_code = LINE;
      return (1);
    }
  return (0);
}

int	click_menu(t_param *param, int x, int y, int bouton)
{
  bouton++;
  if ((x >= 700 && x <= 800) && (y >= 50 && y <= 100))
    return (select_color(param));
  else if ((gest_select_geo(param, x, y)) == 1)
    return (1);
  else if ((x >= 700 && x <= 800) && (y >= 545 && y <= 645))
    return (gest_fill(param));
  else if ((x >= 700 && x <= 800) && (y >= 646 && y <= 740))
    return (gest_thick(param));
  return (0);
}
