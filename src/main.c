/*
** main.c for mytmp in /home/tahar_w/my_paint
**
** Made by tahar_w
** Login   <tahar_w@epitech.net>
**
** Started on  Thu Jun 19 12:57:07 2014 tahar_w
** Last update Mon Jun 23 20:24:19 2014 tahar_w
*/

#include <mlx.h>
#include <stdio.h>
#include <stdlib.h>
#include "draw.h"

int	start_key(int keycode)
{
  if (keycode == 65307)
    exit(EXIT_SUCCESS);
  return (0);
}

void	save_cor_line(int *save_x, int *save_y, int x, int y)
{
  *save_x = x;
  *save_y = y;
}

int		write_pixel(int bouton, int x, int y, t_param *param)
{
  t_draw	pt1;
  t_draw	pt2;
  static int	count = 1;
  static int	save_x = 0;
  static int	save_y = 0;

  if ((click_menu(param, x, y, bouton)) == 1)
    return (0);
  if (param->k_code == LINE && (count % 2) != 0)
    save_cor_line(&save_x, &save_y, x, y);
  else if (param->k_code == LINE && (count % 2) == 0)
    {
      pt1.x = save_x;
      pt1.y = save_y;
      pt2.x = x;
      pt2.y = y;
    }
  else
    {
      count = 1;
      pt1.x = x;
      pt1.y = y;
    }
  count = chose_geo(param, pt1, pt2, count);
  return (0);
}

int	main_window(t_param *param)
{
  param->win_x = 800;
  param->win_y = 800;
  if ((param->mlx_ptr = mlx_init()) == NULL)
    return (-1);
  if ((param->window = mlx_new_window
       (param->mlx_ptr, param->win_x, param->win_y, "Pimp my Paint")) == NULL)
    return (-1);
  wall_init(param);
  color_tools(param);
  mlx_expose_hook(param->window, start_expose, param);
  return (0);
}

int		main()
{
  t_param	param;

  param.k_code = LINE;
  param.thick = SMAL;
  if ((main_window(&param)) == -1)
    return (-1);
  mlx_mouse_hook(param.window, write_pixel, &param);
  mlx_key_hook(param.window, start_key, &param);
  mlx_mouse_hook(param.win_color, &chose_color, &param);
  mlx_key_hook(param.win_color, start_key2, &param);
  mlx_loop(param.mlx_ptr);
  return (0);
}
