/*
** my_wall.c for pimp my paint in /home/tahar_w/my_paint/in_progress/src
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Sat Jun 21 16:24:52 2014 tahar_w
** Last update Sun Jun 22 21:10:21 2014 tahar_w
*/

#include <mlx.h>
#include "draw.h"

void	wall_init(t_param *param)
{
  int	i;

  i = 0;
  param->img_ptr = mlx_new_image(param->mlx_ptr, param->win_x, param->win_y);
  param->data = mlx_get_data_addr(param->img_ptr, &param->bpp, &param->sizeline
				  , &param->endian);
  while (i != param->win_x * param->win_y * 4)
    {
      param->data[i] = 255;
      param->data[i + 1] = 255;
      param->data[i + 2] = 255;
      param->data[i + 3] = 0;
      i = i + 4;
    }
  mlx_put_image_to_window(param->mlx_ptr, param->window,
                          param->img_ptr, 0, 0);
}

void	color_tools(t_param *param)
{
  int	i;

  i = 0;
  param->img_color = mlx_new_image(param->mlx_ptr, 100, 735);
  param->data = mlx_get_data_addr(param->img_color, &param->bpp
				  , &param->sizeline, &param->endian);
  creat_menu(param, &i);
  mlx_put_image_to_window(param->mlx_ptr, param->window,
                          param->img_color, 0, 0);
}
