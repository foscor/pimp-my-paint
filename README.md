# Projet "PIMP MY PAINT" #

Projet de 1er année à Epitech en 3 jours, utilisant la **Minilibx**

### Fonctionalité du projet ###

* tracer des formes simples : Carré, Triangle, Cercle, Segment.
* Palette de couleurs : BLANC, NOIR, ROUGE, BLEU , VERT, JAUNE.
* Epaisseur des trais : Fin , Moyen , Gros.
* Remplissage des formes simples (voir ci-dessus).


# NOTE : 25/30 #

![printf_my_paint.png](https://bitbucket.org/repo/ERRxBK/images/4263228568-printf_my_paint.png)