##
## Makefile for my_tmp in /home/tahar_w/my_paint
## 
## Made by tahar_w
## Login   <tahar_w@epitech.net>
## 
## Started on  Thu Jun 19 13:02:12 2014 tahar_w
## Last update Mon Jun 23 21:30:06 2014 tahar_w
##

NAME=	pmp

CC=	gcc

RM=	rm -f

SRC=	src/main.c		\
	src/draw_line.c		\
	src/creat_menu.c	\
	src/my_wall.c		\
	src/color_menu.c	\
	src/creat_geo.c		\
	src/expose_geo_main.c	\
	src/expose_geo.c	\
	src/chose_geo.c		\
	src/my_thick.c		\
	src/my_fill.c		\
	src/dyna_color_main.c	\
	src/dyna_color.c	\
	src/my_tools.c		\
	src/gest_menu.c		\
	src/my_funct.c

OBJ=	$(SRC:.c=.o)

INCDIR=	include

RCOLOR=	@echo -e "\e[1;31m"
NOCOLO=	@echo -e "\e[0;0m"

CFLAGS  += -Wall -Wextra
CFLAGS  += -I./$(INCDIR) -g3

MLX=   -L/usr/lib64 -lmlx_$(HOSTTYPE) \
        -L/usr/lib64/X11 \
        -lXext \
        -lX11 \
        -lm

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(MLX)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
